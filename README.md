This dotCMS static configuration plugin contains security-related tasks and configuration including constraints requiring portal users to use a secure connection.

## Installation
### From Release
* Download the most recent release archive from [this plugin's repository's tags page](https://gitlab.msu.edu/canr/edu.msu.anr.config.security/tags).
* Extract the release archive to the 'plugins' directory of your dotCMS instance. This should create the directory 'plugins\edu.msu.anr.config.security'.

## Deploying
In order to deploy the configuration plugin to your dotCMS instance, you must run the dotCMS deploy plugins script. Note that this will deploy all of your static plugins.

```
> cd E:\dotcms\dotcms_x.y.z\
> .\bin\deploy-plugins.bat
```
